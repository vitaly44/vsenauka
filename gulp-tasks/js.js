module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src(path.src.js.src)
			.pipe(gulp.dest(path.public.js))
			.pipe(plugins.uglify())
			.pipe(plugins.rename({
				suffix: '.min'
			}))
			.pipe(gulp.dest(path.public.js))
			.pipe(gulp.src(path.src.js.vendors))
			.pipe(plugins.uglify())
			.pipe(gulp.dest(path.public.js))
			.pipe(plugins.connect.reload());
	}
}