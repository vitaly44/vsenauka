$(document).ready(function () {
    $('#addQuize .QQ .list-group').sortable({
        handle: '.drag',
        draggable: '.list-group-item.AA',
        animation: 150,
    });
});

$(document).ready(function () {
    $(document).on('click', '#addQuize #RemQ', function (e) {
        e.preventDefault();

        var _this = $(this);
        var _parent = _this.parents('.QQ');

        _parent.remove();
    });
    $(document).on('click', '#addQuize #RemA', function (e) {
        e.preventDefault();

        var _this = $(this);
        var _parent = _this.parents('.AA');

        _parent.remove();
    });
    $(document).on('click', '#addQuize #RemR', function (e) {
        e.preventDefault();

        var _this = $(this);
        var _parent = _this.parents('.RR');

        _parent.remove();
    });

    $(document).on('click', '#addQuize #MoveADown', function (e) {
        var _this = $(this);
        var _parent = _this.parents('.AA');

        if ($(_parent).next().hasClass('AA')) {
            $(_parent).insertAfter($(_parent).next());
        }

        e.preventDefault();
    });
    $(document).on('click', '#addQuize #MoveAUp', function (e) {
        var _this = $(this);
        var _parent = _this.parents('.AA');

        if (_parent.prev().hasClass('AA')) {
            _parent.insertBefore(_parent.prev());
        }

        e.preventDefault();
    });
    $(document).on('click', '#addQuize #MoveQDown', function (e) {
        var _this = $(this);
        var _parent = _this.parents('.QQ');

        if ($(_parent).next().hasClass('QQ')) {
            $(_parent).insertAfter($(_parent).next());
        }

        e.preventDefault();
    });
    $(document).on('click', '#addQuize #MoveQUp', function (e) {
        var _this = $(this);
        var _parent = _this.parents('.QQ');

        if (_parent.prev().hasClass('QQ')) {
            _parent.insertBefore(_parent.prev());
        }

        e.preventDefault();
    });


    var _addQuestion = $('#addQuize #AddQ');
    var _questionGroup = $('#addQuize .QQ').clone();
    _addQuestion.on('click', function (e) {
        e.preventDefault();

        var _this = $(this);
        var _parent = _this.parent();

        var _cloneE = _questionGroup.clone();
        _parent.before(_cloneE);

        var _qCount = $('#addQuize .QQ').length - 1;

        _cloneE.find('.question label').attr('for', 'qName[' + _qCount + ']');
        _cloneE.find('.question .form-control').attr('id', 'qName[' + _qCount + ']');
        _cloneE.find('.question .form-control').attr('name', 'qName[' + _qCount + ']');
        _cloneE.find('.desc label').attr('for', 'qDesc[' + _qCount + ']');
        _cloneE.find('.desc .form-control').attr('id', 'qDesc[' + _qCount + ']');
        _cloneE.find('.desc .form-control').attr('name', 'qDesc[' + _qCount + ']');


        var _qCount = $('#addQuize .QQ').length - 1;
        var _qACount = _cloneE.find('.AA').length - 1;

        _cloneE.find('.AA .col-form-label').attr('for', 'question[' + _qCount + '][' + _qACount + ']');
        _cloneE.find('.AA .form-control').attr('id', 'question[' + _qCount + '][' + _qACount + ']');
        _cloneE.find('.AA .form-control').attr('name', 'question[' + _qCount + '][' + _qACount + ']');

        _cloneE.find('.AA .custom-control-input').attr('id', 'qTrue[' + _qCount + '][' + _qACount + ']');
        _cloneE.find('.AA .custom-control-input').attr('name', 'qTrue[' + _qCount + ']');
        _cloneE.find('.AA .custom-control-input').attr('value', _qACount);
        _cloneE.find('.AA .custom-control-label').attr('for', 'qTrue[' + _qCount + '][' + _qACount + ']');


        $('#addQuize .QQ .list-group').sortable({
            handle: '.drag',
            draggable: '.list-group-item.AA',
            animation: 150,
        });
    });


    var _answerGroup = $('#addQuize .AA').clone();
    $(document).on('click', '#addQuize #AddAnswer', function (e) {
        e.preventDefault();

        var _this = $(this);
        var _parent = _this.parent();

        var _cloneE = _answerGroup.clone();
        _parent.before(_cloneE);

        var _qCount = $('#addQuize .QQ').index(_cloneE.parents('.QQ'));
        var _qACount = _cloneE.parent().find('.AA').length - 1;

        _cloneE.find('.col-form-label').attr('for', 'question[' + _qCount + '][' + _qACount + ']');
        _cloneE.find('.form-control').attr('id', 'question[' + _qCount + '][' + _qACount + ']');
        _cloneE.find('.form-control').attr('name', 'question[' + _qCount + '][' + _qACount + ']');

        _cloneE.find('.custom-control-input').attr('id', 'qTrue[' + _qCount + '][' + _qACount + ']');
        _cloneE.find('.custom-control-input').attr('name', 'qTrue[' + _qCount + ']');
        _cloneE.find('.custom-control-input').attr('value', _qACount);
        _cloneE.find('.custom-control-label').attr('for', 'qTrue[' + _qCount + '][' + _qACount + ']');
    });


    var _addResult = $('#addQuize #AddResult');
    var _resultGroup = $('#addQuize .RR').clone();
    _addResult.on('click', function (e) {
        e.preventDefault();

        var _this = $(this);
        var _parent = _this.parent();

        var _cloneR = _resultGroup.clone();
        _parent.before(_cloneR);

        var _rCount = $('#addQuize .RR').length - 1;

        _cloneR.find('.inputtext label').attr('for', 'resultMin[' + _rCount + ']');
        _cloneR.find('.inputtext .form-control').attr('id', 'resultMin[' + _rCount + ']');
        _cloneR.find('.inputtext .form-control').attr('name', 'resultMin[' + _rCount + ']');

        _cloneR.find('.textarea.forSite label').attr('for', 'resultText[' + _rCount + ']');
        _cloneR.find('.textarea.forSite .form-control').attr('id', 'resultText[' + _rCount + ']');
        _cloneR.find('.textarea.forSite .form-control').attr('name', 'resultText[' + _rCount + ']');

        _cloneR.find('.textarea.forSocial label').attr('for', 'socialResultText[' + _rCount + ']');
        _cloneR.find('.textarea.forSocial .form-control').attr('id', 'socialResultText[' + _rCount + ']');
        _cloneR.find('.textarea.forSocial .form-control').attr('name', 'socialResultText[' + _rCount + ']');

        _cloneR.find('.inputfile label').attr('for', 'resultImage[' + _rCount + ']');
        _cloneR.find('.inputfile input[type=file]').attr('id', 'resultImage[' + _rCount + ']');
        _cloneR.find('.inputfile input[type=file]').attr('name', 'resultImage[' + _rCount + ']');
    });
});