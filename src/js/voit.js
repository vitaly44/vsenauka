$(document).ready(function () {
    $('.voiting .__questions input').on("change", function () {
        var _this = $(this),
            _thisChecked = _this.is(":checked"),
            _questionItem = _this.parents('.__questions-item'),
            _questions = _this.parents('.__questions'),
            _checkedCount = $('input[type=checkbox]:checked').length,
            _allHead = $('.__head .bg'),
            _loadHead = $('.__head .bgLoading'),
            _thisHead = _allHead.eq(_checkedCount - 1);

        $('.voiting .__questions-item .more-info').removeClass('show');

        _questions.addClass('answered');

        _loadHead.fadeIn(300);

        if (_checkedCount === 0) {
            _questions.removeClass('answered');
        } else {
            _questions.addClass('answered');
        }

        if (_thisChecked) {
            _questionItem.addClass('active');

        } else {
            _questionItem.removeClass('active');
        }

        setTimeout(function () {
            _loadHead.fadeOut(300);

            if (_thisChecked) {
                _thisHead.fadeIn();

            } else {
                _thisHead = _allHead.eq(_checkedCount);
                _thisHead.fadeOut();
            }
        }, 700);

    });
});

$(window).on("load", function () {
    parent.postMessage({
        frameSize: {
            y: $('main').outerHeight()
        }
    }, '*');
});

$(document).ready(function () {
    $('.voiting .btn_next').on("click", function () {

        var _this = $(this),
            _thisBlock = _this.parents('.voiting-block');

        _thisBlock.hide();

        _thisBlock.next().show();

        if (_thisBlock.next().hasClass('voiting-result')) {
            _addRelustTableDownArrow();
        }
    });


    $('.voiting .btn_prev').on("click", function () {

        var _this = $(this),
            _thisBlock = _this.parents('.voiting-block');

        _thisBlock.hide();

        _thisBlock.prev().show();

        if (_thisBlock.next().hasClass('voiting-result')) {
            _addRelustTableDownArrow();
        }
    });
});

function _addRelustTableDownArrow() {
    var _tableResult = $('.__result_table table'),
        _tableRelustHeight = _tableResult.outerHeight(),
        _tableRelustTrHeight = _tableResult.find('tr').outerHeight(),
        _tableRelustShiftHeight = _tableRelustHeight + _tableRelustTrHeight,
        _resultWrap = $('.__result_table-wrap'),
        _resultWrapHeight = _resultWrap.outerHeight();

    _tableRelustShiftHeight > _resultWrapHeight ?
        $('.__result_table-wrap_line').show() :
        $('.__result_table-wrap_line').hide();

}

$(document).ready(function () {
    _addRelustTableDownArrow();
})