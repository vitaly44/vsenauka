function animateCSS(element, animationName, callback) {
    var node = $(element);
    node.removeClass('wow');
    node.prop('style', '');
    node.addClass('animated');
    node.addClass(animationName);

    function handleAnimationEnd() {
        node.removeClass('animated');
        node.removeClass(animationName);
        node.unbind('animationend', handleAnimationEnd);

        if (typeof callback === 'function') callback();
    }

    node.bind('animationend', handleAnimationEnd);
}

$(document).ready(function () {
    var _hamburger = $('.hamburger'),
        _header = $('header');


    _hamburger.on('click', function (e) {
        $(this).toggleClass('is-active');
        _header.toggleClass('menu_show');
    });
});

$(window).scroll(function () {
    var header = $('header'),
        body = $('body'),
        scroll = $(window).scrollTop(),
        top_shift = header.outerHeight() * 2;

    if (scroll >= top_shift) {
        if (!header.hasClass('fixed')) {

            header.addClass('fixed');

            animateCSS(header, 'slideInDown faster');
        }
    } else {
        header.removeClass('fixed');
    }
});

$(document).ready(function () {

    $('#slider1').owlCarousel({
        loop: true,
        dots: true,
        nav: false,
        items: 1,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        onTranslated: function (me) {
            var _this = $(me.target);

            $('.section__title').css('background-image', 'url(' + _this.find('.owl-item.active [data-img]').data('img') + ')');
        }
    });

    $('.block__slider').owlCarousel({
        loop: true,
        dots: false,
        nav: true,
        items: 1,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        navText: ['<i class="far fa-arrow-alt-circle-left"></i>', '<i class="far fa-arrow-alt-circle-right"></i>'],
        autoplayHoverPause: true,
    });

    $('.aside_slider, .imho_slider').owlCarousel({
        loop: true,
        dots: true,
        nav: false,
        items: 1,
        margin: 30,
    });

    $('.list_slider').owlCarousel({
        loop: false,
        dots: false,
        nav: true,
        items: 1,
        margin: 0,
        navText: ['<img src="/img/icons/arrow_left.svg" />', '<img src="/img/icons/arrow_right.svg" />'],
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                margin: 30,
                items: 3,
            }
        }
    });
});

$(document).ready(function () {
    $("article table").wrap("<div class='table-responsive'></div>");
});

$(document).ready(function () {
    $("[data-collapse_more] .theme__list-title").on('click', function (e) {
        var _this = $(this).parent(),
            _isActive = _this.hasClass('active');

        $("[data-collapse_more]").removeClass('active');

        _isActive ?
            _this.removeClass('active') :
            _this.addClass('active');
    });
    $("[data-collapse_more] .hide_more").on('click', function (e) {
        var _this = $(this).parents('.active');

        _this.removeClass('active');
    });
});

$(document).on('click', '.test .test__questions-item input', function (e) {
    var _this = $(this);

    $(".test__questions-item label").removeClass('active');

    _this.parents('.test__questions').addClass('answered');

    _this.is(":checked") ?
        _this.parent().addClass('active') :
        _this.parent().removeClass('active');
});

$(document).ready(function () {
    $('.voiting .__questions .__questions-item').mouseenter(function () {
            if (window.outerWidth > 1024) {
                var _this = $(this);

                _this.addClass('hover');
            }
        })
        .mouseleave(function () {
            if (window.outerWidth > 1024) {
                var _this = $(this);

                _this.removeClass('hover');
            }
        });

    $('.voiting .__questions input').on("change", function () {
        var _this = $(this),
            _thisChecked = _this.is(":checked"),
            _questionItem = _this.parents('.__questions-item'),
            _questionItemColor = _questionItem.find('.color').css('background-color'),
            _questions = _this.parents('.__questions'),
            _checkedCount = $('input[type=checkbox]:checked').length,
            _thisBook = $('.__head .book').eq(_checkedCount - 1);

        _questions.addClass('answered');

        _checkedCount === 0 ?
            _questions.removeClass('answered') :
            _questions.addClass('answered')

        if (_thisChecked) {
            _questionItem.addClass('active');
            _thisBook.css('opacity', 1);
            _thisBook.css('background-color', _questionItemColor);

        } else {
            _questionItem.removeClass('active');
            _thisBook = $('.__head .book').eq(_checkedCount);
            _thisBook.css('opacity', 0);
        }

        $('.voiting .__questions-item .more-info').removeClass('show');
    });
});

function hide_submenu() {
    $('nav .nav__links-item.links .submenu').removeClass('show');
    $('nav .nav__links-item.links .btnlink').removeClass('is_submenu');
}
$(document).ready(function () {
    $("header nav .nav__links-item.links .btnlink i").on('click', function () {
        var _this = $(this),
            _parent = _this.parent(),
            _allItems = _parent.parent(),
            _subMenu = _parent.find('ul'),
            _subMenuIsShow = _subMenu.hasClass('show');

        _allItems.find('.nav__links-item ul').removeClass('show');
        if (_subMenuIsShow) {
            _parent.removeClass('is_submenu');
            _subMenu.removeClass('show');
        } else {
            _parent.addClass('is_submenu');
            _subMenu.addClass('show');
        }
    });

    $("header nav .nav__links-item.links .btnlink").hover(
        function () {
            var _this = $(this),
                _allItems = _this.parent(),
                _subMenu = _this.find('ul');

            _allItems.find('.nav__links-item .submenu').removeClass('show');

            if (window.outerWidth > 992) {
                _this.addClass('is_submenu');
                _subMenu.addClass('show');
            }
        },
        function () {
            var _this = $(this),
                _allItems = _this.parent(),
                _subMenu = _this.find('ul');

            _allItems.find('.nav__links-item .submenu').removeClass('show');

            if (window.outerWidth > 992) {
                _this.removeClass('is_submenu');
                _subMenu.removeClass('show');
            }
        }
    );

    $('html, body').on('click', function () {
        var _body = $('body');
        var eventInMenu = $(event.target).parents('header');

        if (!eventInMenu.length) {
            hide_submenu();
            $(".hamburger").removeClass("is-active");
        }
    })
});

$(window).scroll(function () {
    var _isSubMenuClose = false;
    var _subMenu = $('.is_submenu');

    _isSubMenuClose = _subMenu.length;

    if (window.innerWidth > 1025 || !_isSubMenuClose) {
        hide_submenu();
    }
});


$(document).ready(function () {
    $('.voiting_exp .get-info').on('click', function () {
        var _this = $(this),
            _parent = _this.parent();

        _parent.find('.more-info').toggleClass('show');
    });


    $('html, body').on('click', function () {
        var eventInMenu = $(event.target).parents('.voiting_exp h2');

        if (!eventInMenu.length) {
            $('.voiting_exp .more-info').removeClass('show');
        }
    })
});


$(document).on('click', '.voiting .__questions-item .cr', function (e) {

    var _this = $(this),
        _parent = _this.parent(),
        _moreInfo = _parent.find('.more-info'),
        _isShowMore = _moreInfo.hasClass('show');

    $('.voiting .__questions-item .more-info').removeClass('show');


    _isShowMore ?
        _moreInfo.removeClass('show') :
        _moreInfo.addClass('show');
});

$(document).ready(function () {
    $('html, body').on('click', function () {
        var eventInItem = $(event.target).parents('.voiting .__questions-item');

        if (!eventInItem.length) {
            $('.voiting .__questions-item .more-info').removeClass('show');
        }
    })
});

$(document).ready(function () {
    var _sticky = $('.__sticky'),
        _sticky_height = $('.__sticky').outerHeight();

    var _sticky_block = $('.__sticky-stop_block'),
        _sticky_block_offset = _sticky_block.offset(),
        _offset_top = _sticky_block_offset.top;

    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop(),
            _headerHeight = $('header').outerHeight(),
            _sticky_block_height = _sticky_block.height(),
            _offset_bottom = _offset_top + _sticky_block_height - _sticky_height,
            _sticky_block_width = _sticky_block.outerWidth(),
            _marginTop = _headerHeight;

        if (scrollTop >= _offset_top && scrollTop <= _offset_bottom) {
            console.log('в блоке');

            _sticky.addClass('__sticky-fixed');

            _sticky_block.css({
                'margin-top': _sticky_height
            })

            _sticky.css({
                'width': _sticky_block_width,
                'top': _marginTop,
            });
        } else {
            console.log('вне блока');

            _sticky.removeClass('__sticky-fixed');

            _sticky_block.css({
                'margin-top': 0
            })

            _sticky.css({
                width: '100%',
                top: 0,
            });
        }
    });
});