var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')(),
	ftp = require('vinyl-ftp'),
	pug = require('gulp-pug'),
	sass = require('gulp-sass'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	connect = require('gulp-connect'),
	af = require('gulp-autoprefixer'),
	plumber = require('gulp-plumber'),
	notify = require('gulp-notify'),
	uglify = require('gulp-uglify'),
	typograf = require('gulp-typograf'),
	sourcemaps = require('gulp-sourcemaps'),
	del = require('del'),
	gulpif = require('gulp-if'),
	revts = require('gulp-version-number'),
	src = "./src",
	public = "./public",
	port = "8001";

var path = {
	public: {
		root: public,
		all: public + '/**/*',
		html: public + '/',
		htmlAll: public + '/**/*.html',
		js: public + '/js/',
		css: public + '/css/',
		cssAll: public + '/css/**/*.css',
		img: public + '/img/',
		fonts: public + '/fonts/'
	},
	src: {
		html: src + '/html/*.pug',
		htmlAll: src + '/html/**/*.html',
		js: {
			all: src + '/js/**/*',
			src: src + '/js/*.js',
			vendors: src + '/js/vendors/*.js'
		},
		scss: {
			all: src + '/scss/**/*.scss',
			vendors: src + '/scss/vendors/*.scss',
			src: src + '/scss/*.scss',
		},
		css: src + '/css/**/*.css',
		img: src + '/img/**/*.*',
		fonts: src + '/fonts/**/*.*'
	},
	watch: {
		pug: src + '/html/**/*.pug',
		html: src + '/html/**/*.html',
		js: src + '/js/**/*.js',
		scss: src + '/scss/**/*.scss',
		css: src + '/css/**/*.css',
		img: src + '/img/**/*.*',
		fonts: src + '/fonts/**/*.*'
	}
};

var af_opts = {
	browsers: [
		'last 3 versions',
		'iOS >= 8',
		'Android >= 4.2',
		'Explorer >= 11',
		'ExplorerMobile >= 11'
	]
};


plugins.notify = notify;
plugins.rename = rename;
plugins.sass = sass;
plugins.cleanCSS = cleanCSS;
plugins.ftp = ftp;
plugins.del = del;
plugins.uglify = uglify;
plugins.plumber = plumber;
plugins.af = af;
plugins.af_opts = af_opts;
plugins.pug = pug;
plugins.typograf = typograf;
plugins.sourcemaps = sourcemaps;
plugins.gulpif = gulpif;
plugins.revts = revts;


function getTask(task) {
	return require('./gulp-tasks/' + task)(gulp, plugins, path);
}

gulp.task('clean', getTask('clean'));

gulp.task('zip', getTask('zip'));

gulp.task('ftp', getTask('ftp'));

gulp.task('js', getTask('js'));
gulp.task('sass', getTask('sass'));
gulp.task('minCSS', getTask('minifyCSS'));
gulp.task('html', getTask('html'));
gulp.task('revts', getTask('revTimeStamp'));
gulp.task('typograf', getTask('typograf'));

gulp.task('copy', () => {
	gulp.src(path.src.htmlAll)
		.pipe(gulp.dest(path.public.html));

	gulp.src(path.src.css)
		.pipe(gulp.dest(path.public.css));

	gulp.src(path.src.img)
		.pipe(gulp.dest(path.public.img));

	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.public.fonts))
		.pipe(plugins.connect.reload());
});

gulp.task('watch', async () => {
	gulp.watch(path.watch.scss, gulp.series('sass'));
	gulp.watch(path.watch.js, gulp.series('js'));
	gulp.watch(path.watch.pug, gulp.series('html'));
	gulp.watch([
			path.watch.css,
			path.watch.img,
			path.watch.fonts,
			path.watch.html,
		],
		gulp.series('copy'));
});

gulp.task('connect', async () => {
	connect.server({
		root: public,
		port: port,
		fallback: public + '/index1.html',
		livereload: true
	});
});

gulp.task('build', gulp.series('clean', 'js', 'sass', 'html', 'copy', 'revts'));
gulp.task('prod', gulp.series('clean', 'js', 'sass', 'minCSS', 'html', 'copy', 'revts'));
gulp.task('default', gulp.series('build', 'watch', 'connect'));
gulp.task('deploy', gulp.series('prod', 'ftp'));